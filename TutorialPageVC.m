//
//  TutorialPageVC.m
//  sswguide
//
//  Created by Administrador on 11/26/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "TutorialPageVC.h"
#import "TutorialContentPageVC.h"
#import "CollectionViewController.h"

@interface TutorialPageVC ()

@end

@implementation TutorialPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _pageTitles = @[@"Selecciona tu Categoria", @"Escoge tu Favorito", @"Conoce su Descripcion"];
    _pageImages = @[@"page1.png", @"page2.png", @"page3.png"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialPageVC"];
    self.pageViewController.dataSource = self;
    
    TutorialContentPageVC *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:0.800 alpha:1.000];
    pageControl.currentPageIndicatorTintColor = [UIColor yellowColor];
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentPageVC*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentPageVC*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (TutorialContentPageVC *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    TutorialContentPageVC *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialContentPageVC"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (IBAction)star:(id)sender {
    
    CollectionViewController *categorias = [self.storyboard instantiateViewControllerWithIdentifier: @"nVC1"];
    categorias.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:categorias animated:YES completion:nil];
//    [self.navigationController pushViewController:categorias animated:YES];
    
    
}
@end
