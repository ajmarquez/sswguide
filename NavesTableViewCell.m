//
//  NavesTableViewCell.m
//  sswguide
//
//  Created by Administrador on 10/1/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "NavesTableViewCell.h"

@implementation NavesTableViewCell

@synthesize imagenNave, nameNave, creadorNave, by;

- (void)awakeFromNib {
    // Initialization code
    
 
    
   
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"naveCell"];
    if (self) {
        
       
     
        
        by.translatesAutoresizingMaskIntoConstraints=NO;
        
        imagenNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        nameNave.translatesAutoresizingMaskIntoConstraints = NO;
        creadorNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview: imagenNave];
        
        //imagen
        
        NSLayoutConstraint *alto = [NSLayoutConstraint constraintWithItem:imagenNave attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
        
        [self addConstraint:alto];
        
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:imagenNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:10];
        
        [self addConstraint:top];
        
        NSLayoutConstraint *deImg = [NSLayoutConstraint constraintWithItem:imagenNave attribute:NSLayoutAttributeLeadingMargin relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        [self  addConstraint:deImg];
        
        
        NSLayoutConstraint *ancho = [NSLayoutConstraint constraintWithItem:imagenNave attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeWidth multiplier:0.3 constant:0];
        
        [self addConstraint:ancho];
        
        [ self addSubview: by];
        //by
        
        NSLayoutConstraint *izBy= [NSLayoutConstraint constraintWithItem:by attribute:NSLayoutAttributeLeftMargin relatedBy:NSLayoutRelationEqual toItem:imagenNave attribute:NSLayoutAttributeRight multiplier:1.0 constant:50];
        
        [self addConstraint:izBy];
        
        NSLayoutConstraint *deBy= [NSLayoutConstraint constraintWithItem:by attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeRight multiplier:1.0 constant:5];
        
        [self addConstraint:deBy];
        
        
        
        
        
        NSLayoutConstraint *posicionY =[NSLayoutConstraint constraintWithItem:by attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:imagenNave attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        
        [self  addConstraint:posicionY];
        
        
        
        //
        //
        //    // nombre
        
        [ self addSubview: nameNave];
        
        NSLayoutConstraint *topName = [NSLayoutConstraint constraintWithItem:nameNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:15];
        
        [self addConstraint:topName];
        
        
        
        NSLayoutConstraint *bottomBy = [NSLayoutConstraint constraintWithItem:nameNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                       toItem:by attribute:NSLayoutAttributeTop multiplier:1.0 constant:-5];
        
        [self addConstraint:bottomBy];
        
        NSLayoutConstraint *izName = [NSLayoutConstraint constraintWithItem:nameNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:imagenNave attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:10];
        
        [self  addConstraint:izName];
        
        
        
        NSLayoutConstraint *deName = [NSLayoutConstraint constraintWithItem:nameNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10];
        
        
        [self.contentView addConstraint:deName];
        
        //
        //    //creador
        
        
        
            NSLayoutConstraint *topCreador = [NSLayoutConstraint constraintWithItem:creadorNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:by attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10];
        
            [ self addConstraint:topCreador];
        
            NSLayoutConstraint *izCreador = [NSLayoutConstraint constraintWithItem:creadorNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:imagenNave attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:10];
        
            [self addConstraint:izCreador];
        
        
        
            NSLayoutConstraint *deCreador = [NSLayoutConstraint constraintWithItem:creadorNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10];
            
            
            [self addConstraint:deCreador];
            
            
            
            
            
            
            
            NSLayoutConstraint *bCreador = [NSLayoutConstraint constraintWithItem:creadorNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
            
            [self addConstraint:bCreador];
        
        
        
        
    }
    return self;
}





@end
