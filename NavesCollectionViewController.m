//
//  NavesCollectionViewController.m
//  sswguide
//
//  Created by Administrador on 10/19/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "NavesCollectionViewController.h"
#import "NavesCollectionViewCell.h"

#import "NavesCollectionViewCell2.h"
#include "DescriptionViewController.h"

@interface NavesCollectionViewController ()

@end

@implementation NavesCollectionViewController

static NSString * const reuseIdentifier = @"CellNave";

static NSString * const reuseIdentifier2 = @"CellNave2";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
   label.font= [UIFont fontWithName:@"Star Jedi Outline" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = @"Naves";
    self.navigationItem.titleView = label;
     
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
   // [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    
    
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"Starships" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filepath];
    //NSLog(@"%@",filepath);
    
    NSError *error = nil;
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    if (error != nil)
    {
        NSLog(@"ERROR: %@", [error localizedDescription]);
    }
    else {
        
        //NSLog(@"%@",jsonDic);
        naves= jsonArray;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
#define kCellsPerRow 2
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    CGFloat availableWidthForCells = CGRectGetWidth(self.collectionView.frame) - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (kCellsPerRow - 1);
    CGFloat cellWidth = availableWidthForCells ;
    
    
    CGFloat avilableHeightForCell = CGRectGetHeight (self.collectionView.frame) - flowLayout.sectionInset.top - flowLayout.sectionInset.bottom - flowLayout.minimumInteritemSpacing * (3) - flowLayout.minimumLineSpacing * (3);
    CGFloat cellHeight = avilableHeightForCell / 3.33;
    
    //NSLog( @"ALTO : %f", avilableHeightForCell);
    
    
    flowLayout.itemSize = CGSizeMake(cellWidth, cellHeight);
    
    [self.collectionView registerClass:[NavesCollectionViewCell class]
forCellWithReuseIdentifier:reuseIdentifier];
    
    
    [self.collectionView registerClass:[NavesCollectionViewCell2 class]
            forCellWithReuseIdentifier:reuseIdentifier2];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [naves count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 == 0) {
    
    NavesCollectionViewCell *cell = (NavesCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSDictionary *nave= [naves objectAtIndex:indexPath.row];
    
    
    cell.nameNave.text = [nave objectForKey:@"name"];
    cell.nameNave.numberOfLines= 2;
    cell.by.text =@"By";
    cell.imagenNave.image= [UIImage imageNamed:@"X-wing_SWGTCG.jpg"];
    cell.creadorNave.text=[nave objectForKey:@"manufacturer"];
    cell.creadorNave.numberOfLines= 3;
    
    return cell;
    } else{
        
        
        NavesCollectionViewCell2 *cell = (NavesCollectionViewCell2*)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier2 forIndexPath:indexPath];
        NSDictionary *nave= [naves objectAtIndex:indexPath.row];
        
        
        cell.nameNave.text = [nave objectForKey:@"name"];
        cell.nameNave.numberOfLines= 2;
        cell.by.text =@"By";
        cell.imagenNave.image= [UIImage imageNamed:@"9273832-large.jpg"];
        cell.creadorNave.text=[nave objectForKey:@"manufacturer"];
        cell.creadorNave.numberOfLines= 3;
        
                               return cell;
        }
                               
                               
                               
        }
#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/


// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 
 DescriptionViewController *desNavesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"DescriptionViewController"];
 [self.navigationController pushViewController:desNavesVC animated:YES];
 
    NSDictionary *nave= [naves objectAtIndex:indexPath.row];
    
    [desNavesVC loadView];
    [desNavesVC.nameNave setText:[nave objectForKey:@"name"]];
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font= [UIFont fontWithName:@"Star Jedi Outline" size:10];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = [nave objectForKey:@"name"];
    desNavesVC.navigationItem.titleView = label;
    
    

    if (indexPath.row % 2 == 0) {
    
    
    [desNavesVC.imagenNave setImage:[UIImage imageNamed:@"X-wing_SWGTCG.jpg"]];
        
    }else{
        
        [desNavesVC.imagenNave setImage:[UIImage imageNamed:@"9273832-large.jpg"]];
        
    }
 
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
