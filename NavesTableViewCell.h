//
//  NavesTableViewCell.h
//  sswguide
//
//  Created by Administrador on 10/1/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavesTableViewCell : UITableViewCell
@property (weak, nonatomic, readwrite) IBOutlet UIImageView *imagenNave;
@property (weak, nonatomic,readwrite) IBOutlet UILabel *nameNave;
@property (weak, nonatomic) IBOutlet UILabel *creadorNave;
@property (weak, nonatomic) IBOutlet UILabel *by;

- (id)initWithFrame:(CGRect)frame;

@end
