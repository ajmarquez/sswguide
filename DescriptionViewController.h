//
//  DescriptionViewController.h
//  sswguide
//
//  Created by Administrador on 10/23/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imagenNave;
@property (weak, nonatomic) IBOutlet UILabel *nameNave;
@property (weak, nonatomic) IBOutlet UITextView *textoNave;

@end
