//
//  NavesCollectionViewCell2.m
//  sswguide
//
//  Created by Administrador on 10/22/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "NavesCollectionViewCell2.h"
#define ContainerWidth self.contentView.bounds.size.width

@implementation NavesCollectionViewCell2



@synthesize imagenNave = _imagenNave;
@synthesize nameNave = _nameNave;
@synthesize by= _by;
@synthesize creadorNave=_creadorNave;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor blackColor];
        
        
        
        // imagen de la nave
        
        _imagenNave = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, ContainerWidth*0.50,self.contentView.bounds.size.height)];
        [self.contentView addSubview:_imagenNave];
        
        _imagenNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        
        
        

        
                NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_imagenNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:10];
        
                [self addConstraint:top];
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_imagenNave attribute:NSLayoutAttributeBottomMargin relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10];
        
        [self addConstraint:bottom];
        
        NSLayoutConstraint *deImg = [NSLayoutConstraint constraintWithItem:_imagenNave attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10];
        
        [self  addConstraint:deImg];
        
        
        NSLayoutConstraint *ancho = [NSLayoutConstraint constraintWithItem:_imagenNave attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0];
        
        [self addConstraint:ancho];
        
        
        // by
        
        _by = [[UILabel alloc] initWithFrame:CGRectMake(ContainerWidth*0.50, 35, ContainerWidth*0.50, 40)];
        [self.contentView addSubview:_by];
        
        _by.textColor= [UIColor whiteColor];
        
        _by.font= [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        
        _by.translatesAutoresizingMaskIntoConstraints=NO;
        
        NSLayoutConstraint *izBy= [NSLayoutConstraint constraintWithItem:_by attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:_imagenNave attribute:NSLayoutAttributeLeft  multiplier:1.0 constant:-(ContainerWidth/4)];
        
        [self addConstraint:izBy];
        
        //        NSLayoutConstraint *deBy= [NSLayoutConstraint constraintWithItem:_by attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeRight multiplier:1.0 constant:-5];
        //
        //        [self addConstraint:deBy];
        
        
        
        NSLayoutConstraint *posicionY =[NSLayoutConstraint constraintWithItem:_by attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        
        [self  addConstraint:posicionY];
        
        
        
        // nombre de la nave
        
        
        _nameNave = [[UILabel alloc] initWithFrame:CGRectMake(ContainerWidth*0.50, 20, ContainerWidth*0.50, 40)];
        
        
        [self.contentView addSubview:_nameNave];
        _nameNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        _nameNave.textColor= [UIColor whiteColor];
        _nameNave.font= [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _nameNave.numberOfLines = 2;
        _nameNave.textAlignment = NSTextAlignmentCenter;
        _nameNave.minimumScaleFactor= 0.1;
        
        
        /*
         NSLayoutConstraint *topName = [NSLayoutConstraint constraintWithItem:_nameNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:20];
         
         [self addConstraint:topName];
         */
        
        
        NSLayoutConstraint *bottomBy = [NSLayoutConstraint constraintWithItem:_nameNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                       toItem:_by attribute:NSLayoutAttributeTop multiplier:1.0 constant:-15];
        
        [self addConstraint:bottomBy];
        
        NSLayoutConstraint *izName = [NSLayoutConstraint constraintWithItem:_nameNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_imagenNave attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-10];
        
        [self  addConstraint:izName];
        
        
        
        NSLayoutConstraint *deName = [NSLayoutConstraint constraintWithItem:_nameNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        
        [self addConstraint:deName];
        
        // creador de la nave
        
        
        
        _creadorNave = [[UILabel alloc] initWithFrame:CGRectMake(ContainerWidth*0.50, 20, ContainerWidth*0.50, 40)];
        
        
        [self.contentView addSubview:_creadorNave];
        _creadorNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        
        
        _creadorNave.textColor= [UIColor whiteColor];
        _creadorNave.font= [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        _creadorNave.numberOfLines = 3;
        _creadorNave.minimumScaleFactor= 0.1;
        _creadorNave.textAlignment = NSTextAlignmentCenter;
        
        
        NSLayoutConstraint *topCreador = [NSLayoutConstraint constraintWithItem:_creadorNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:_by attribute:NSLayoutAttributeBottom multiplier:1.0 constant:12];
        
        [self addConstraint:topCreador];
        
        NSLayoutConstraint *izCreador = [NSLayoutConstraint constraintWithItem:_creadorNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_imagenNave attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-10];
        
        [self addConstraint:izCreador];
        
        
        
        NSLayoutConstraint *deCreador = [NSLayoutConstraint constraintWithItem:_creadorNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        
        [self addConstraint:deCreador];
        
        
        NSLayoutConstraint *bCreador = [NSLayoutConstraint constraintWithItem:_creadorNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
        
        [self addConstraint:bCreador];
        
        
        
    }
    return self;
}


@end
