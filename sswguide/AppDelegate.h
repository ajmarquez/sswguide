//
//  AppDelegate.h
//  sswguide
//
//  Created by Administrador on 9/28/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

