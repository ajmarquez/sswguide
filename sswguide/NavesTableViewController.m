//
//  NavesTableViewController.m
//  sswguide
//
//  Created by Administrador on 10/1/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "NavesTableViewController.h"
#import "NavesTableViewCell.h"


@interface NavesTableViewController ()

@end

@implementation NavesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"Starships" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:filepath];
    //NSLog(@"%@",filepath);
    
    NSError *error = nil;
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                            options:kNilOptions
                                                              error:&error];
    if (error != nil)
    {
        NSLog(@"ERROR: %@", [error localizedDescription]);
    }
    else {
        
        //NSLog(@"%@",jsonDic);
        naves= jsonArray;
    }

    
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [naves  count];
}


- (NavesTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   NSDictionary *nave= [naves objectAtIndex:indexPath.row];
    
    NavesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier :@"naveCell" forIndexPath:indexPath];
    
    
    
    //if (indexPath.row % 2 == 0) {
    
    
       
        
     /*
        cell.imagenNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        cell.nameNave.translatesAutoresizingMaskIntoConstraints = NO;
        cell.creadorNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        //imagen
        
        NSLayoutConstraint *alto = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
        
        [cell addConstraint:alto];
        
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTop multiplier:1.0 constant:10];
        
        [cell addConstraint:top];
        
        NSLayoutConstraint *deImg = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeLeadingMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        [cell addConstraint:deImg];

        
        NSLayoutConstraint *ancho = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0];
        
         [cell addConstraint:ancho];
        
        
        //by
        
       NSLayoutConstraint *izBy= [NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeLeftMargin relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeRight multiplier:1.0 constant:50];
        
        [cell addConstraint:izBy];
        
        NSLayoutConstraint *deBy= [NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeRight multiplier:1.0 constant:-50];
        
        [cell addConstraint:deBy];

        
        
        
        
NSLayoutConstraint *posicionY =[NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        
        [cell addConstraint:posicionY];
        
      
        
      
        
          // nombre
        
        NSLayoutConstraint *topName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTop multiplier:1.0 constant:15];
        
        [cell addConstraint:topName];
        
        
        
        NSLayoutConstraint *bottomBy = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                       toItem:cell.by attribute:NSLayoutAttributeTop multiplier:1.0 constant:-5];
       
        [cell addConstraint:bottomBy];
        
        NSLayoutConstraint *izName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:10];
        
        [cell addConstraint:izName];
        
        
        
        NSLayoutConstraint *deName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10];
        
        
        [cell addConstraint:deName];
        
        
        //creador
        
       
        
        NSLayoutConstraint *topCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell.by attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10];
        
        [cell addConstraint:topCreador];
        
        NSLayoutConstraint *izCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:10];
        
        [cell addConstraint:izCreador];
        
        
        
        NSLayoutConstraint *deCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10];
        
        
        [cell addConstraint:deCreador];
        
        
        
        
        
        
        
        NSLayoutConstraint *bCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationLessThanOrEqual toItem:cell attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
        
        [cell addConstraint:bCreador];
        
        */
        
        
        
    cell.nameNave.text = [nave objectForKey:@"name"];
        cell.nameNave.numberOfLines= 2;
        cell.imagenNave.image= [UIImage imageNamed:@"X-wing_SWGTCG.jpg"];
    cell.creadorNave.text=[nave objectForKey:@"manufacturer"];
        cell.creadorNave.numberOfLines= 3;
        return cell;
        
    //} else {
        /*
        
        NavesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nave2Cell" forIndexPath:indexPath];
        
        cell.imagenNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        cell.nameNave.translatesAutoresizingMaskIntoConstraints = NO;
        cell.creadorNave.translatesAutoresizingMaskIntoConstraints = NO;
        
        
        //imagen
        
        NSLayoutConstraint *alto = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
        
        [cell addConstraint:alto];
        
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTop multiplier:1.0 constant:10];
        
        [cell addConstraint:top];
        
        NSLayoutConstraint *deImg = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-10];
        
        [cell addConstraint:deImg];
        
        
        NSLayoutConstraint *ancho = [NSLayoutConstraint constraintWithItem:cell.imagenNave attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0];
        
        [cell addConstraint:ancho];
        
        //by
        
        NSLayoutConstraint *deBy= [NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeRightMargin relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-50];
        
        [cell addConstraint:deBy];

        NSLayoutConstraint *izBy= [NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeLeftMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeLeft multiplier:1.0 constant:50];
        
        [cell addConstraint:izBy];
                
        NSLayoutConstraint *posicionY =[NSLayoutConstraint constraintWithItem:cell.by attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        
        [cell addConstraint:posicionY];
        
        
        
        
        
        // nombre
        
        NSLayoutConstraint *topName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeTop multiplier:1.0 constant:15];
        
        [cell addConstraint:topName];
        
        
        
        NSLayoutConstraint *bottomBy = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                       toItem:cell.by attribute:NSLayoutAttributeTop multiplier:1.0 constant:-5];
        
        [cell addConstraint:bottomBy];
        
        NSLayoutConstraint *izName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        [cell addConstraint:izName];
        
        
        
        NSLayoutConstraint *deName = [NSLayoutConstraint constraintWithItem:cell.nameNave attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-10];
        
        
        [cell addConstraint:deName];
        
        
        //creador
        
        
        
        NSLayoutConstraint *topCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:cell.by attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10];
        
        [cell addConstraint:topCreador];

        NSLayoutConstraint *deCreador  = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeTrailingMargin relatedBy:NSLayoutRelationEqual toItem:cell.imagenNave attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-10];
        

[cell addConstraint:deCreador];
        
        
        NSLayoutConstraint *izCreador= [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeLeadingMargin relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10];
        
        

        
        [cell addConstraint:izCreador];
        
        
        
        
        
        NSLayoutConstraint *bCreador = [NSLayoutConstraint constraintWithItem:cell.creadorNave attribute:NSLayoutAttributeBottomMargin  relatedBy:NSLayoutRelationLessThanOrEqual toItem:cell attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
        
        [cell addConstraint:bCreador];
        
        
        
        
        cell.nameNave.text = [nave objectForKey:@"name"];
        cell.nameNave.numberOfLines = 2;
        cell.imagenNave.image= [UIImage imageNamed:@"9273832-large.jpg"];
    
        cell.creadorNave.text=[nave objectForKey:@"manufacturer"];
        cell.creadorNave.numberOfLines = 3;
        return cell;
    }
    */
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
