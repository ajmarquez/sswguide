//
//  TutorialContentPageVC.h
//  sswguide
//
//  Created by Administrador on 11/26/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialContentPageVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;



@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;


@end
