//
//  CollectionViewController.m
//  sswguide
//
//  Created by Administrador on 9/29/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "NavesTableViewController.h"
#import "NavesCollectionViewController.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bannerMain.png"]];
    
    
    
    sections = [[NSMutableArray alloc]init];
    
    //Especies, Planetas, Naves y Vehículos.
    
    NSDictionary *planetas = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"planetas.png",@"Planetas", nil] forKeys:[NSArray arrayWithObjects:@"img",@"name", nil]];
    
    [sections addObject:planetas];
    
    NSDictionary *especies = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"especies.png",@"Especies", nil] forKeys:[NSArray arrayWithObjects:@"img",@"name", nil]];
    
    [sections addObject:especies];
    
    NSDictionary *naves = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"naves.png",@"Naves", nil] forKeys:[NSArray arrayWithObjects:@"img",@"name", nil]];
    
    [sections addObject:naves];
    
    
    NSDictionary *vehiculos = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"vehiculos.png",@"Vehiculos", nil] forKeys:[NSArray arrayWithObjects:@"img",@"name", nil]];
    
    [sections addObject:vehiculos];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
   //[self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    
    #define kCellsPerRow 2
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    CGFloat availableWidthForCells = CGRectGetWidth(self.collectionView.frame) - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (kCellsPerRow - 1);
    CGFloat cellWidth = availableWidthForCells / kCellsPerRow;
    
    
    CGFloat avilableHeightForCell = CGRectGetHeight (self.collectionView.frame) - flowLayout.sectionInset.top - flowLayout.sectionInset.bottom - flowLayout.minimumInteritemSpacing * (3) - flowLayout.minimumLineSpacing * (3);
    CGFloat cellHeight = avilableHeightForCell / 3.33;

    //NSLog( @"ALTO : %f", avilableHeightForCell);
    
    
    flowLayout.itemSize = CGSizeMake(cellWidth, cellHeight);

    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [sections count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *section = [sections objectAtIndex:indexPath.row];
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
 
    cell.layer.borderWidth = 2;
    cell.layer.borderColor= UIColor.blackColor.CGColor;
    
    cell.name.text= [section objectForKey:@"name"];
    cell.img.image= [UIImage imageNamed:[section objectForKey:@"img"]];
    
    return cell;
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(10,10,10,10);  // top, left, bottom, right
}


#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/


 //Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *section = [sections objectAtIndex:indexPath.row];
    
    
    switch (indexPath.row) {
        case 2 :
      NSLog(@"%@",[section objectForKey:@"name"]);
        NavesCollectionViewController *navesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"NavesCollectionViewController"];
    [self.navigationController pushViewController:navesVC animated:YES];
        
        
        break;
        
        }
    //NSLog(@" %ld",(long)indexPath.row );
    
    

    
    
    
    
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
