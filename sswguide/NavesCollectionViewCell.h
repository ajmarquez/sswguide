//
//  NavesCollectionViewCell.h
//  sswguide
//
//  Created by Administrador on 10/19/15.
//  Copyright © 2015 Singular. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavesCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong, readwrite) IBOutlet UIImageView *imagenNave;
@property (nonatomic,strong, readwrite) IBOutlet UILabel *nameNave;
@property (nonatomic,strong, readwrite) IBOutlet UILabel *by;
@property (nonatomic,strong, readwrite) IBOutlet UILabel *creadorNave;

@end
